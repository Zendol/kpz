using NUnit.Framework;
using Robot.Common;
using System.Collections.Generic;
using Moq;
using YartymVitalii.RobotChallenge;

namespace ApplicationTest
{
    [TestFixture]
    public class AlgorithmTest
    {
        private YartymVitaliiAlgorithm algorithm;
        private Map map;
        private List<EnergyStation> stations;
        private Robot.Common.Robot movingRobot;


        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            algorithm = new YartymVitaliiAlgorithm();
            
            stations = new List<EnergyStation>()
            {
                new EnergyStation() {Energy = 1000, Position = new Position(3,3), RecoveryRate = 2},
                new EnergyStation() {Energy = 1000, Position = new Position(15,10), RecoveryRate = 1},
                new EnergyStation() {Energy = 1000, Position = new Position(8,9), RecoveryRate = 3},
                new EnergyStation() {Energy = 1000, Position = new Position(5,9), RecoveryRate = 4},
                new EnergyStation() {Energy = 1000, Position = new Position(3,5), RecoveryRate = 2},
            };
            map = new Map() { Stations = stations};
            movingRobot = new Robot.Common.Robot() { Position = new Position(4, 4) };
        }

        [SetUp]
        public void Setup()
        {
            movingRobot = new Robot.Common.Robot() { Position = new Position(4, 4) };
        }

        [TestCase(500)]
        [TestCase(350)]
        public void HasNearbyResEnoughEnergy_EnergyEnough_ReturnsTrue(int energy)
        {
            var result = algorithm.HasNearbyResEnoughEnergy(movingRobot, map, energy);

            Assert.IsTrue(result);
        }

        [TestCase(500)]
        public void HasNearbyResEnoughEnergy_NotEnough_ReturnsFalse(int energy)
        {
            var farFromEnergyRobot = new Robot.Common.Robot() { Position = new Position(50, 50) };

            var result = algorithm.HasNearbyResEnoughEnergy(farFromEnergyRobot, map, energy);

            Assert.IsFalse(result);
        }

        [Test]
        public void CalculateEnergy_StationNearbyExist_ReturnsEnergyOfNearbyStations()
        {
            var result = algorithm.CalculateEnergy(movingRobot, map);

            Assert.Greater(result, 0);
        }

        [Test]
        public void CalculateEnergy_StationNotExists_ReturnsEnergyOfNearbyStations()
        {
            var farFromEnergyRobot = new Robot.Common.Robot() { Position = new Position(50, 50) };

            var result = algorithm.CalculateEnergy(farFromEnergyRobot, map);

            Assert.Zero(result);
        }

        [Test]
        public void GetFreeStations_StationExists_ReturnsListOfStations()
        {
            var robots = new List<Robot.Common.Robot>() 
            {
                new Robot.Common.Robot() { Position = new Position(50, 50) 
            }};

            var result = algorithm.GetFreeStations(stations, robots);

            Assert.AreEqual(stations, result);
        }

        [Test]
        public void GetFreeStations_AllStationsWithRobots_ReturnsEmptyList()
        {
            var robots = new List<Robot.Common.Robot>();
            stations.ForEach(station => robots.Add(new Robot.Common.Robot() { Position = station.Position }));

            var result = algorithm.GetFreeStations(stations, robots);

            Assert.IsEmpty(result);
        }

        [Test]
        public void DoStep_EnergyEnoughToCreateRobot_ReturnsCreateRobotCommand()
        {
            movingRobot.Energy = 1300;
            var robots = new List<Robot.Common.Robot>()
            {
                movingRobot,
                new Robot.Common.Robot() { Position = new Position(50, 50)
            }};

            var result = algorithm.DoStep(robots, robots.IndexOf(movingRobot), map);

            Assert.IsInstanceOf(typeof(CreateNewRobotCommand), result);
        }

        [Test]
        public void DoStep_CantCreateAndNearbyEnergyEnough_ReturnsCollectCommand()
        {
            movingRobot.Energy = 1000;
            var robots = new List<Robot.Common.Robot>()
            {
                movingRobot,
                new Robot.Common.Robot() { Position = new Position(50, 50)
            }};

            var result = algorithm.DoStep(robots, robots.IndexOf(movingRobot), map);

            Assert.IsAssignableFrom<CollectEnergyCommand>(result);
        }

        [Test]
        public void DoStep_CantCreateAndNearbyEnergyNotEnough_CalledAttackMoveFunc()
        {
            var algorithmMock = new Mock<YartymVitaliiAlgorithm>();
            movingRobot.Energy = 1300;
            var robots = new List<Robot.Common.Robot>()
            {
                movingRobot,
                new Robot.Common.Robot() { Position = new Position(50, 50)
            }};

            var result = algorithmMock.Object.DoStep(robots, 1, map);

            algorithmMock.Verify(obj => obj.Attack_Move(robots, 1, map), Times.Once);
        }

        [Test]
        public void AttackMove_EnemyAttacked_ReturnsMoveCommandWithPositionOfEnemy()
        {
            movingRobot.Position = new Position(51, 53);
            movingRobot.Energy = 1500;
            var enemy = new Robot.Common.Robot() { Energy = 1000, Position = new Position(52, 55) };
            var robots = new List<Robot.Common.Robot>() { movingRobot, enemy };

            algorithm.Round = 15;

            var result = algorithm.Attack_Move(robots, robots.IndexOf(movingRobot), map);

            Assert.IsInstanceOf(typeof(MoveCommand), result);
        }

        [Test]
        public void CalculateCenterPoint_StaionsCount1_ReturnsPosOfFirstStation()
        {
            var pos = new Position(10, 10);
            var stations = new List<EnergyStation>() { new EnergyStation() { Position = pos } };

            var result = algorithm.CalculateCenterPoint(stations);

            Assert.AreSame(pos, result);
        }

        [Test]
        public void CalculateCenterPoint__ReturnsPosOfFirstStation()
        {
            var expectedPosition = new Position(6, 7);

            var result = algorithm.CalculateCenterPoint(stations);

            Assert.AreEqual(expectedPosition, result);
        }
    }
}