﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace YartymVitalii.RobotChallenge
{
    public static class Helper
    {
        public static int Distance { get; set; } = 4;
        public static int EnergyToCreateNewRobot { get; set; } = 1200;
        public static int StopCreateRobotRound { get; set; } = 37;
        public static int MaxRadiusToFind { get; set; } = 41;
        public static int MaxMove { get; set; } = 11;
        public static double PercentEnergyAttack { get; set; } = 0.3;
        public static int EnergyForAttack { get; set; } = 20;

        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
        public static bool IsCollectable(EnergyStation station, Position position)
        {
            return Math.Max((Math.Abs(station.Position.X - position.X)),
                (Math.Abs(station.Position.Y - position.Y))) <= Distance;
        }
    }

    public class YartymVitaliiAlgorithm : IRobotAlgorithm
    {
        public string Author { get; set; } = "Yartym Vitalii";
        public int Round { get; set; } = 0;

        public YartymVitaliiAlgorithm()
        {
            Logger.OnLogRound += Logger_OnRound;
        }

        void Logger_OnRound(object sender, LogRoundEventArgs a)
        {
            Round++;
        }


        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var movingRobot = robots[robotToMoveIndex];
            if (Round < Helper.StopCreateRobotRound &&
                movingRobot.Energy > Helper.EnergyToCreateNewRobot &&
                robots.Where(robot => robot.OwnerName == Author).Count() != 100)
            {
                return new CreateNewRobotCommand() { NewRobotEnergy = 100 };
            }
            else if (HasNearbyResEnoughEnergy(robots[robotToMoveIndex], map, 500))
            {
                return new CollectEnergyCommand();
            }
            else
            {
                return Attack_Move(robots, robotToMoveIndex, map);
            }
        }

        public bool HasNearbyResEnoughEnergy(Robot.Common.Robot robot, Map map, int energyEnough)
        {
            return map.GetNearbyResources(robot.Position, 4).Sum(station => station.Energy) >= energyEnough;
        }

        public int CalculateEnergy(Robot.Common.Robot robot, Map map)
        {
            return map.GetNearbyResources(robot.Position, 4).Sum(station => station.Energy);
        }

        public List<EnergyStation> GetCollectableStations(int radius, IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var stations = map.GetNearbyResources(robots[robotToMoveIndex].Position, radius);

            var freeStaions = GetFreeStations(stations, robots);

            return freeStaions;
        }

        public List<EnergyStation> GetFreeStations(List<EnergyStation> stations ,IList<Robot.Common.Robot> robots)
        {
            return stations.Where(station =>
            {
                foreach (var robot in robots)
                {
                    if (Helper.IsCollectable(station, robot.Position))
                    {
                        return false;
                    }
                }
                return true;
            }).ToList();
        }


        public int EnergyForMove(Position position1, Position position2)
        {
            return (int)Math.Pow(position1.X - position2.X, 2) + (int)Math.Pow(position1.Y - position2.Y, 2);
        }

        public bool IsAttackGreaterCollect(Robot.Common.Robot robot, Robot.Common.Robot enemy,  Map map)
        {
            int energyCollect = CalculateEnergy(robot, map);
            int energyAttack = Convert.ToInt32(enemy.Energy * Helper.PercentEnergyAttack -
                Helper.EnergyForAttack + EnergyForMove(robot.Position, enemy.Position));
            return energyAttack > energyCollect;
        }

        public RobotCommand MoveToStation(List<EnergyStation> stations, Robot.Common.Robot movingRobot)
        {
            stations.OrderBy(x => EnergyForMove(movingRobot.Position, x.Position));

            foreach (var station in stations)
            {
                for (int i = 1; i <= Helper.MaxMove; i++)
                {
                    Position move = movingRobot.Position.Copy();
                    move.X = (move.X * (i - 1) + station.Position.X) / i;
                    move.Y = (move.Y * (i - 1) + station.Position.Y) / i;

                    if (EnergyForMove(movingRobot.Position, move) * i <= movingRobot.Energy)
                    {
                        return new MoveCommand() { NewPosition = move };
                    }
                }
            }
            return new MoveCommand();
        }

        public virtual RobotCommand Attack_Move(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var movingRobot = robots[robotToMoveIndex];
            int radius = Helper.MaxRadiusToFind;

            if (GetCollectableStations(Helper.Distance, robots, robotToMoveIndex, map).Count == 0 && Round > 5)
            {
                var enemies = robots.Where(robot => robot.OwnerName != Author)
                                    .OrderBy(robot => EnergyForMove(robot.Position, movingRobot.Position))
                                    .ThenByDescending(robot => robot.Energy)
                                    .ToList();
                
                foreach (var enemy in enemies)
                {
                    if (enemy.Energy * Helper.PercentEnergyAttack > Helper.EnergyForAttack + EnergyForMove(movingRobot.Position, enemy.Position)
                        && IsAttackGreaterCollect(movingRobot,enemy,map))
                    {
                        return new MoveCommand() { NewPosition = enemy.Position };
                    }
                }
            }

            if (HasNearbyResEnoughEnergy(movingRobot, map, 300))
            {
                return new CollectEnergyCommand();
            }

            var collectableStations = GetCollectableStations(radius, robots, robotToMoveIndex, map);
            if (collectableStations.Count >= 1)
            {
                return MoveToStation(collectableStations, movingRobot);
            }

            var bestStationPosition = GetBestFreeStation(movingRobot, map, robots);
            if (bestStationPosition == null)
            {
                return new MoveCommand() { NewPosition = new Position(movingRobot.Position.X + 2, movingRobot.Position.Y + 2) };
            }
            return new MoveCommand() { NewPosition = bestStationPosition };
        }

        public Position GetBestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            var closestStations = map.Stations.OrderBy(station => Helper.FindDistance(station.Position, movingRobot.Position))
                                              .ThenByDescending(station => station.Energy)
                                              .ToList();

            var freeStations = GetFreeStations(closestStations, robots);

            var bestStation = freeStations.First();

            var stationsNearBest = freeStations.Where(station => Helper.FindDistance(bestStation.Position, station.Position) <= 16).ToList();

            if(stationsNearBest.Count < 1)
            {
                return null;
            }
            return CalculateCenterPoint(stationsNearBest);
        }

        public  Position CalculateCenterPoint(List<EnergyStation> stations)
        {
            if (stations.Count() == 1)
            {
                return stations[0].Position;
            }
            else
            {
                int x = 0;
                int y = 0;
                for (int i = 0, n = stations.Count; i < n; i++)
                {
                    x += stations[i].Position.X;
                    y += stations[i].Position.Y;
                }
                int x_centr = x / stations.Count();
                int y_centrs = y / stations.Count();

                return new Position(x_centr, y_centrs);
            }
        }
    }
}



